package ca.cegepdrummond;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class Test_Serie2_Variables extends SimulConsole {
    @Test
    @Order(1)
    void test_variable1() throws Exception {
        choixMenu("2a");
        assertSortie("1234", false);

    }

    @Test
    @Order(2)
    void test_variable2() throws Exception {
        choixMenu("2b");
        assertSortie("3 4 5", false);
        
    }
    
    @Test
    @Order(3)
    void test_variable3() throws Exception {
        choixMenu("2c");
        assertSortie("42", false);
        
    }
    
    @Test
    @Order(4)
    void test_variable4() throws Exception {
        choixMenu("2d");
        assertSortie("a", false);
        
    }


    
    
}
